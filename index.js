/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/	
	//Codes for no.1
	function sumTwoNumber (num1, num2){
		let sum= (num1+num2);
		console.log("Displayed sum of " + num1 + " and " + num2);
		console.log(sum);
	}

	function minusTwoNumber (num3, num4){
		let difference =(num3-num4);
		console.log("Displayed difference of " + num3 + " and " +num4);
		console.log(difference);
	}

	sumTwoNumber(num1 = 5, num2 =15);
	minusTwoNumber(num3=20, num4=5);


/*	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/
	//Codes for no.2

	function multiplyTwoNumber (num5, num6){
		console.log("The product of " + num5 + " and " + num6);
		return num5 * num6;

	}

	function divideTwoNumber (num7, num8){
		
		console.log("The quotient of " + num7 + " and " + num8);
		return num7/num8;
	}

	let product = multiplyTwoNumber(50,10 );
	console.log(product);
	let quotient = divideTwoNumber(50,10);
	console.log(quotient);

/*	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/
	//code for no.3 
	function totalAreaCircle (numRadius){
		
		console.log("The result of getting the area of a circle with " + numRadius + " radius:");
		return 3.1415*(numRadius**2);
	}

	let areaCircle = totalAreaCircle(15);
	console.log(areaCircle);

/*	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/	
	//code for no.4
	function totalAverage (grades1,grades2,grades3,grades4){
		console.log("The average of "+ grades1 + ", "+ grades2+ ", "+ grades3 + " and "+ grades4);
		return (grades1+grades2+grades3+grades4)/4;
	}

	let average = totalAverage(20,40,60,80);
	console.log(average);
/*	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
	//code for no.5 
	function percentageScore (score, totalScore){
		console.log("Is "+score+"/"+totalScore+ " a passing score?");
		let percent = (score/totalScore)*100;
		let isPassing = percent >= 75;
		return isPassing;
	}

	let isPassingScore = percentageScore(38, 50);
	console.log(isPassingScore);
